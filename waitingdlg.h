#ifndef WAITINGDLG_H
#define WAITINGDLG_H

#include <QDialog>
#include <QThread>
#include "waitingdlg.h"

namespace Ui {
class WaitingDlg;
}
class Worker;


class WaitingDlg : public QDialog
{
    Q_OBJECT

public:
    explicit WaitingDlg(QWidget *parent = 0);
    ~WaitingDlg();

private:
    Ui::WaitingDlg *ui;
    QThread* pTh;
    Worker* pWork;

public slots:
    void onWorkerCompleted(void);
};

class Worker : public QObject
{
    Q_OBJECT

public:
    Worker();
    ~Worker();
    void doSetUp(QThread* pTh, WaitingDlg* pDlg);

private slots:
    void onStarted(void);

signals:
    void finished(void);
};

#endif // WAITINGDLG_H
