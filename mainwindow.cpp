#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _pWaitDlg = nullptr;

    connect(ui->btnStart,
            SIGNAL(clicked(bool)),
            SLOT(popUpWaitingDlg()));
}

MainWindow::~MainWindow()
{
    delete ui;
    if(_pWaitDlg != nullptr)
    {
        delete _pWaitDlg;
    }
}

void MainWindow::popUpWaitingDlg()
{
    if(_pWaitDlg != nullptr)
    {
        delete _pWaitDlg;
        _pWaitDlg = nullptr;
    }

    _pWaitDlg = new WaitingDlg();
    _pWaitDlg->exec();
}
