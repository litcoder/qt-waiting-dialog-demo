#include <QThread>
#include <QDebug>
#include "waitingdlg.h"
#include "ui_waitingdlg.h"
#include "waitingdlg.h"

WaitingDlg::WaitingDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WaitingDlg)
{
    ui->setupUi(this);

    connect(ui->btnCancel,
            SIGNAL(clicked(bool)),
            SLOT(close()));

    pTh = new QThread();
    pWork = new Worker();
    pWork->doSetUp(pTh, this);


    pWork->moveToThread(pTh);
    pTh->start();
}

WaitingDlg::~WaitingDlg()
{
    delete ui;
    delete pTh;
    delete pWork;
}

void WaitingDlg::onWorkerCompleted()
{
    qDebug() << "Worker thread complted!";
    pTh->terminate();
    this->close();
}

Worker::Worker()
{

}

Worker::~Worker()
{

}

void Worker::doSetUp(QThread* pTh, WaitingDlg *pDlg)
{
    connect(pTh,
            SIGNAL(started()),
            this,
            SLOT(onStarted()));

    connect(this,
            SIGNAL(finished()),
            pDlg,
            SLOT(onWorkerCompleted()));

}

void Worker::onStarted()
{
    // Some heavy work.
    QThread::sleep(5);
    emit(finished());
}

