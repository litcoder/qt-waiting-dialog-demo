#-------------------------------------------------
#
# Project created by QtCreator 2015-08-18T21:25:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTWaitingDlgDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    waitingdlg.cpp

HEADERS  += mainwindow.h \
    waitingdlg.h

FORMS    += mainwindow.ui \
    waitingdlg.ui
